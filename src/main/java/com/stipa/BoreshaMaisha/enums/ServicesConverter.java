package com.stipa.BoreshaMaisha.enums;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.util.stream.Stream;

@Converter(autoApply = true)
public class ServicesConverter implements AttributeConverter<Services, String> {

    @Override
    public String convertToDatabaseColumn(Services services) {
        if (services == null) {
            return null;
        }
        return services.getCode();
    }

    @Override
    public Services convertToEntityAttribute(String code) {
        if (code == null) {
            return null;
        }

        return Stream.of(Services.values())
                .filter(c -> c.getCode().equals(code))
                .findFirst()
                .orElseThrow(IllegalArgumentException::new);
    }
}
