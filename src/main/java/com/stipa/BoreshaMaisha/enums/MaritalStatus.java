package com.stipa.BoreshaMaisha.enums;

public enum MaritalStatus {
    MARRIED("M"), SINGLE("S"), DIVORCED("D");

    private String code;

    private MaritalStatus(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }
}
