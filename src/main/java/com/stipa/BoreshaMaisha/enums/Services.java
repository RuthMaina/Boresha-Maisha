package com.stipa.BoreshaMaisha.enums;

public enum Services {
    INPATIENT("I"), OUTPATIENT("O");

    private String code;

    public String getCode() {
        return code;
    }

    private Services(String code) {
        this.code = code;
    }
}
