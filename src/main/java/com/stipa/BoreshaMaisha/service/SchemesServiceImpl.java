package com.stipa.BoreshaMaisha.service;

import com.stipa.BoreshaMaisha.NotFoundException;
import com.stipa.BoreshaMaisha.models.Counties;
import com.stipa.BoreshaMaisha.models.Schemes;
import com.stipa.BoreshaMaisha.repositories.CountiesRepository;
import com.stipa.BoreshaMaisha.repositories.SchemesRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class SchemesServiceImpl implements SchemesService {
    private final SchemesRepository schemesRepository;
    private final CountiesRepository countiesRepository;

    public SchemesServiceImpl(SchemesRepository schemesRepository, CountiesRepository countiesRepository) {
        this.schemesRepository = schemesRepository;
        this.countiesRepository = countiesRepository;
    }

    @Override
    public Page<Schemes> findAll(Pageable page) {
        return schemesRepository.findAll(page);
    }

    @Override
    public Schemes findById(Long id) {
        return schemesRepository.findById(id).orElseThrow(() ->
                new NotFoundException("No record with id " + id + " found"));
    }

    @Override
    public Page<Schemes> findByName(String name, Pageable page) {
        return schemesRepository.findByNameContainingIgnoreCase(name, page);
    }

    @Override
    public Schemes create(Schemes schemes,Long countyId) {
        return countiesRepository.findById(countyId).map(county -> {
            schemes.setCounties(county);
            return schemesRepository.save(schemes);
        }).orElseThrow(() -> new NotFoundException("CountyId " + countyId + " not found"));
    }

    @Override
    public Long delete(Long id) {
        try {
            schemesRepository.deleteById(id);
            return id;
        } catch (Exception e) {
            e.getStackTrace();
        }
        return (long) 0;
    }

    @Override
    public Schemes update(Schemes schemes,Long countyId, Long schemeId) {
        return schemesRepository.findById(schemeId).map(scheme -> {
           Counties counties = countiesRepository.findById(countyId).orElseThrow(() ->
                    new NotFoundException("County ID " + countyId + " not found"));;
            scheme.setName(schemes.getName());
            scheme.setSubCounty(schemes.getSubCounty());
            scheme.setVillage(schemes.getVillage());
            scheme.setCode(schemes.getCode());
            scheme.setCounties(counties);
            return schemesRepository.save(scheme);
        }).orElseThrow(() -> new NotFoundException("County ID " + countyId + "not found"));
    }
}
