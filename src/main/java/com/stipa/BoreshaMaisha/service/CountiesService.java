package com.stipa.BoreshaMaisha.service;


import com.stipa.BoreshaMaisha.models.Counties;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Map;

public interface CountiesService {
    Page<Counties> findAll(Pageable page);

    Counties findById(Long id);

    Page<Counties> findByName(String name, Pageable page);

    Counties create(Counties counties,Long PhaseId);

    Long delete(Long id);

    Counties update(Counties counties, Long PhaseId,Long countyId);
}
