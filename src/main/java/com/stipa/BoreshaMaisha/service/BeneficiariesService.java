package com.stipa.BoreshaMaisha.service;


import com.stipa.BoreshaMaisha.models.Beneficiaries;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface BeneficiariesService {
    Page<Beneficiaries> findAll(Pageable page);

    Beneficiaries findById(String id);

    Beneficiaries createPrinciple(String code, Beneficiaries beneficiaries);

    Beneficiaries createBeneficiary(String code, Beneficiaries beneficiaries);

    String delete(String id);

    Beneficiaries update(String id, Beneficiaries beneficiaries);
}
