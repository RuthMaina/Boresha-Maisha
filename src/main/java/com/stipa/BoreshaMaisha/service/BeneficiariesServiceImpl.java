package com.stipa.BoreshaMaisha.service;

import com.stipa.BoreshaMaisha.NotFoundException;
import com.stipa.BoreshaMaisha.models.Beneficiaries;
import com.stipa.BoreshaMaisha.models.Households;
import com.stipa.BoreshaMaisha.models.Schemes;
import com.stipa.BoreshaMaisha.repositories.BeneficiariesRepository;
import com.stipa.BoreshaMaisha.repositories.HouseholdsRepository;
import com.stipa.BoreshaMaisha.repositories.SchemesRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import javax.validation.Valid;

@Service
public class BeneficiariesServiceImpl implements BeneficiariesService {

    private final BeneficiariesRepository beneficiariesRepository;
    private final HouseholdsRepository householdsRepository;
    private final SchemesRepository schemesRepository;

    public BeneficiariesServiceImpl(BeneficiariesRepository beneficiariesRepository, HouseholdsRepository householdsRepository, SchemesRepository schemesRepository) {
        this.beneficiariesRepository = beneficiariesRepository;
        this.householdsRepository = householdsRepository;
        this.schemesRepository = schemesRepository;
    }

    @Override
    public Page<Beneficiaries> findAll(Pageable page) {

        return beneficiariesRepository.findAll(page);
    }

    @Override
    public Beneficiaries findById(String id) {
        return (Beneficiaries) beneficiariesRepository.findById(id).orElseThrow(() ->
                new NotFoundException("No record with id " + id + " found"));
    }

    @Override
    public Beneficiaries createPrinciple(String code, Beneficiaries beneficiaries) {
        if (beneficiariesRepository.findByIdNumber(beneficiaries.getIdNumber()).isPresent()){
            return beneficiariesRepository.findByIdNumber(beneficiaries.getIdNumber()).get();
        }

        Households households = new Households();
        Schemes schemes = schemesRepository.findByCode(code).orElseThrow(() -> new NotFoundException("No scheme of code " + code + " exists"));
        households.setSchemes(schemes);
        Households result = householdsRepository.save(households);
        String newCode = result.getId();

        households = householdsRepository.findById(newCode).orElseThrow(() -> new NotFoundException("No household of id " + newCode + " exists"));
        beneficiaries.setHouseholds(households);
        return beneficiariesRepository.save(beneficiaries);
    }

    @Override
    public Beneficiaries createBeneficiary(String code, Beneficiaries beneficiaries) {
        if (beneficiariesRepository.findByIdNumber(beneficiaries.getIdNumber()).isPresent()){
            return beneficiariesRepository.findByIdNumber(beneficiaries.getIdNumber()).get();
        }
        Households households = householdsRepository.findById(code).orElseThrow(() -> new NotFoundException("No household of id " + code + " exists"));
        beneficiaries.setHouseholds(households);
        return beneficiariesRepository.save(beneficiaries);
    }

    @Override
    public String delete(String id) {
        try {
            beneficiariesRepository.deleteById(id);
            return id;
        } catch (Exception e) {
            e.getStackTrace();
        }
        return null;
    }

    @Override
    public Beneficiaries update(String id, @RequestBody Beneficiaries beneficiaries) {
        Beneficiaries foundBeneficiaries = findById(id);

        foundBeneficiaries.setHouseholds(beneficiaries.getHouseholds());
        foundBeneficiaries.setNames(beneficiaries.getNames());
        foundBeneficiaries.setIdNumber(beneficiaries.getIdNumber());
        foundBeneficiaries.setDateOfBirth(beneficiaries.getDateOfBirth());
        foundBeneficiaries.setAge(beneficiaries.getAge());
        foundBeneficiaries.setGender(beneficiaries.getGender());
        foundBeneficiaries.setMaritalStatus(beneficiaries.getMaritalStatus());
        foundBeneficiaries.setPhone(beneficiaries.getPhone());
        foundBeneficiaries.setIncomeLevel(beneficiaries.getIncomeLevel());
        foundBeneficiaries.setEducationLevel(beneficiaries.getEducationLevel());
        foundBeneficiaries.setLivelihood(beneficiaries.getLivelihood());
        foundBeneficiaries.setVslParticipation(beneficiaries.getVslParticipation());
        foundBeneficiaries.setAgricultureParticipation(beneficiaries.getAgricultureParticipation());
        foundBeneficiaries.setUhcMembership(beneficiaries.isUhcMembership());
        foundBeneficiaries.setUhcNUmber(beneficiaries.getUhcNUmber());
        foundBeneficiaries.setNhifMembership(beneficiaries.isNhifMembership());
        foundBeneficiaries.setNhifNumber(beneficiaries.getNhifNumber());
        foundBeneficiaries.setRole(beneficiaries.getRole());
        return beneficiariesRepository.save(foundBeneficiaries);
    }
}
