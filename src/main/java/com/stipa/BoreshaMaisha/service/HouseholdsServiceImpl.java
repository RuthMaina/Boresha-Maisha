package com.stipa.BoreshaMaisha.service;

import com.stipa.BoreshaMaisha.NotFoundException;
import com.stipa.BoreshaMaisha.models.Households;
import com.stipa.BoreshaMaisha.repositories.HouseholdsRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class HouseholdsServiceImpl implements HouseholdsService {
    private final HouseholdsRepository householdsRepository;

    public HouseholdsServiceImpl(HouseholdsRepository householdsRepository) {
        this.householdsRepository = householdsRepository;
    }

    @Override
    public Page<Households> findAll(Pageable page) {
        return householdsRepository.findAll(page);
    }

    @Override
    public Households findById(String id) {
        return (Households) householdsRepository.findById(id).orElseThrow(() ->
                new NotFoundException("No record with id " + id + " found"));
    }

    @Override
    public Households create(Households households) {
        return householdsRepository.save(households);
    }

    @Override
    public String delete(String id) {
        try {
            householdsRepository.deleteById(id);
            return id;
        } catch (Exception e) {
            e.getStackTrace();
        }
        return null;
    }

    @Override
    public Households update(String id, Households households) {
        Households foundHouseholds = findById(id);
        foundHouseholds.setVpgs(households.getVpgs());
        return householdsRepository.save(foundHouseholds);
    }
}
