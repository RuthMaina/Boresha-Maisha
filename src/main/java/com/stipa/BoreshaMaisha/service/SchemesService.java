package com.stipa.BoreshaMaisha.service;

import com.stipa.BoreshaMaisha.models.Schemes;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface SchemesService {

    Page<Schemes> findAll(Pageable page);

    Schemes findById(Long id);

    Page<Schemes> findByName(String name, Pageable page);

    Schemes create(Schemes schemes,Long countyId);

    Long delete(Long id);

    Schemes update(Schemes schemes,Long countyId,Long schemeId);
}
