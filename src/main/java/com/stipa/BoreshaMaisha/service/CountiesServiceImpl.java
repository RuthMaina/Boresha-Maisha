package com.stipa.BoreshaMaisha.service;

import com.stipa.BoreshaMaisha.NotFoundException;
import com.stipa.BoreshaMaisha.models.Counties;
import com.stipa.BoreshaMaisha.models.Phases;
import com.stipa.BoreshaMaisha.repositories.CountiesRepository;
import com.stipa.BoreshaMaisha.repositories.PhasesRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.nio.file.LinkOption;

@Service
public class CountiesServiceImpl implements CountiesService {

    private final CountiesRepository countiesRepository;
    private final PhasesRepository phasesRepository;

    public CountiesServiceImpl(CountiesRepository countiesRepository, PhasesRepository phasesRepository) {
        this.countiesRepository = countiesRepository;
        this.phasesRepository = phasesRepository;
    }

    @Override
    public Page<Counties> findAll(Pageable page) {
        return countiesRepository.findAll(page);
    }

    @Override
    public Counties findById(Long id) {
        return countiesRepository.findById(id).orElseThrow(() ->
                new NotFoundException("No record with id " + id + " found"));
    }

    @Override
    public Counties create(Counties counties,Long phaseId) {
        return phasesRepository.findById(phaseId).map(phase -> {
            counties.setPhases(phase);
            return countiesRepository.save(counties);
        }).orElseThrow(() -> new NotFoundException("PhaseId " + phaseId + " not found"));

    }

    @Override
    public Long delete(Long id) {
        try {
            countiesRepository.deleteById(id);
            return id;
        } catch (Exception e) {
            e.getStackTrace();
        }
        return (long) 0;
    }

    @Override
    public Counties update(Counties counties, Long phaseId, Long countyId) {
        return countiesRepository.findById(countyId).map(county -> {
            Phases phase = phasesRepository.findById(phaseId).orElseThrow(() ->
                    new NotFoundException("Phase ID " + phaseId + " not found"));;
            county.setName(counties.getName());
            county.setCode(counties.getCode());
            county.setPhases(phase);
            return countiesRepository.save(county);
        }).orElseThrow(() -> new NotFoundException("County ID " + countyId + "not found"));
    }


    @Override
    public Page<Counties> findByName(String name, Pageable page) {
        return countiesRepository.findByNameContainingIgnoreCase(name, page);
    }
}
