package com.stipa.BoreshaMaisha.service;

import com.stipa.BoreshaMaisha.NotFoundException;
import com.stipa.BoreshaMaisha.models.Facilities;
import com.stipa.BoreshaMaisha.repositories.FacilitiesRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class FacilitiesServiceImpl implements FacilitiesService {
    private final FacilitiesRepository facilitiesRepository;

    public FacilitiesServiceImpl(FacilitiesRepository facilitiesRepository) {
        this.facilitiesRepository = facilitiesRepository;
    }

    @Override
    public Page<Facilities> findAll(Pageable page) {
        return facilitiesRepository.findAll(page);
    }

    @Override
    public Facilities findById(Long id) {
        return facilitiesRepository.findById(id).orElseThrow(() ->
                new NotFoundException("No record with id " + id + " found"));
    }

    @Override
    public Page<Facilities> findByAddress(String name, Pageable page) {
        return facilitiesRepository.findByAddressContainingIgnoreCase(name, page);
    }

    @Override
    public Facilities create(Facilities facilities) {
        return facilitiesRepository.save(facilities);
    }

    @Override
    public Long delete(Long id) {
        try {
            facilitiesRepository.deleteById(id);
            return id;
        } catch (Exception e) {
            e.getStackTrace();
        }
        return (long) 0;
    }

    @Override
    public Facilities update(Facilities facilities) {
        Facilities foundFacilities = findById(facilities.getId());
        return getFacilities(facilities, foundFacilities);
    }

    @Override
    public Facilities update(Long id, Facilities facilities) {
        Facilities foundFacilities = findById(id);
        return getFacilities(facilities, foundFacilities);
    }

    private Facilities getFacilities(Facilities facilities, Facilities foundFacilities) {
        foundFacilities.setLevel(facilities.getLevel());
        foundFacilities.setAddress(facilities.getAddress());
        foundFacilities.setContactPerson(facilities.getContactPerson());
        foundFacilities.setSettlementTerms(facilities.getSettlementTerms());
        foundFacilities.setServices(facilities.getServices());
        foundFacilities.setMouStatus(facilities.getMouStatus());
        foundFacilities.setReferralFacility(facilities.getReferralFacility());
        return facilitiesRepository.save(foundFacilities);
    }
}
