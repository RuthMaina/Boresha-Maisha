package com.stipa.BoreshaMaisha.service;

import com.stipa.BoreshaMaisha.models.Vpgs;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface VpgsService {

    Page<Vpgs> findAll(Pageable page);

    Vpgs findById(Long id);

    Page<Vpgs> findByName(String name, Pageable page);

    Vpgs create(Vpgs vpgs);

    Long delete(Long id);

    Vpgs update(Long id, Vpgs vpgs);
}
