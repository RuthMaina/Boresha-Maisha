package com.stipa.BoreshaMaisha.service;

import com.stipa.BoreshaMaisha.models.Users;

import com.stipa.BoreshaMaisha.repositories.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class JwtUserDetailsService implements UserDetailsService {
    @Autowired
    private com.stipa.BoreshaMaisha.repositories.UsersRepository user;

    @Autowired
    private PasswordEncoder scryptEncoder;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Users user = this.user.findByUsername(username);
        if (user == null) {
            throw new UsernameNotFoundException("User not found with username: " + username);
        }
        return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(), new ArrayList<>());
    }

    public Users save(Users user) {
        Users userExists = this.user.findByUsername(user.getUsername());
        if (userExists != null){
            return new Users(user.getUsername(), user.getPassword(), user.getRole());
        } else {
            Users newUser = new Users();
            newUser.setUsername(user.getUsername());
            newUser.setPassword(scryptEncoder.encode(user.getPassword()));
            newUser.setRole(user.getRole());
            return this.user.save(newUser);
        }
    }

    public Users update(String userUsername, Users users){
        Users updateUser = this.user.findByUsername(userUsername);
        if (user == null) {
            throw new UsernameNotFoundException("User not found with username: " + userUsername);
        }
        updateUser.setPassword(users.getPassword());
        updateUser.setRole(users.getRole());
        return this.user.save(updateUser);
    }
}
