package com.stipa.BoreshaMaisha.service;

import com.stipa.BoreshaMaisha.models.Phases;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.time.Year;

public interface PhasesService {

    Page<Phases> findAll(Pageable page);

    Page<Phases> findByStartYear(String year, Pageable page);

    Phases findById(Long id);

    Phases create(Phases phases);

    Long delete(Long id);

    Phases update(Phases phases);

    Phases update(Long id, Phases phases);
}
