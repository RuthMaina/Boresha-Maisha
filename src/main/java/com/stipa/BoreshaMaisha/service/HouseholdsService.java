package com.stipa.BoreshaMaisha.service;

import com.stipa.BoreshaMaisha.models.Households;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface HouseholdsService {

    Page<Households> findAll(Pageable page);

    Object findById(String id);

    Households create(Households households);

    String delete(String id);

    Households update(String id, Households households);
}
