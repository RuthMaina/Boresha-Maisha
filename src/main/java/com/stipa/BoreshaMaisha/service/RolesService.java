package com.stipa.BoreshaMaisha.service;

import com.stipa.BoreshaMaisha.models.Roles;

import java.util.List;

public interface RolesService {
    List<Roles> findAll();

    Roles findById(String id);

    Roles create(Roles roles);

//    String delete(String id);
}
