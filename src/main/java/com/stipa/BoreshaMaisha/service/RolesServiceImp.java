package com.stipa.BoreshaMaisha.service;

import com.stipa.BoreshaMaisha.NotFoundException;
import com.stipa.BoreshaMaisha.models.Roles;
import com.stipa.BoreshaMaisha.repositories.RolesRepository;
import org.apache.commons.text.WordUtils;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public class RolesServiceImp implements RolesService {
    private final RolesRepository rolesRepository;

    public RolesServiceImp(RolesRepository rolesRepository) {
        this.rolesRepository = rolesRepository;
    }

    @Override
    public List<Roles> findAll() {
        return rolesRepository.findAll();
    }

    @Override
    public Roles findById(String id) {
        return (Roles) rolesRepository.findById(id.toLowerCase()).orElseThrow(() -> new NotFoundException("No record with id " + id + " found"));
    }

    @Override
    public Roles create(Roles roles) {
        if (rolesRepository.findById(roles.getRole().toLowerCase()).isPresent()) {
            return (Roles) rolesRepository.findById(roles.getRole().toLowerCase()).get();
        }

        roles.setRole(WordUtils.capitalizeFully(roles.getRole(), ' ', '_', '-', '.'));
        roles.setId(roles.getRole().toLowerCase());
        return rolesRepository.save(roles);
    }
}
