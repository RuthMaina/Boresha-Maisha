package com.stipa.BoreshaMaisha.service;

import com.stipa.BoreshaMaisha.models.Facilities;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface FacilitiesService {

    Page<Facilities> findAll(Pageable page);

    Facilities findById(Long id);

    Page<Facilities> findByAddress(String name, Pageable page);

    Facilities create(Facilities facilities);

    Long delete(Long id);

    Facilities update(Facilities facilities);

    Facilities update(Long id, Facilities facilities);
}
