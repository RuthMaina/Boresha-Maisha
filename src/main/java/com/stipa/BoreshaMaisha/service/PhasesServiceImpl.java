package com.stipa.BoreshaMaisha.service;

import com.stipa.BoreshaMaisha.NotFoundException;
import com.stipa.BoreshaMaisha.models.Phases;
import com.stipa.BoreshaMaisha.repositories.PhasesRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.time.Year;

@Service
public class PhasesServiceImpl implements PhasesService {
    private final PhasesRepository phasesRepository;

    public PhasesServiceImpl(PhasesRepository phasesRepository) {
        this.phasesRepository = phasesRepository;
    }

    @Override
    public Page<Phases> findAll(Pageable page) {
        return phasesRepository.findAll(page);
    }

    @Override
    public Page<Phases> findByStartYear(String year, Pageable page) {
        return phasesRepository.findByStartYearContaining(year,page);
    }

    @Override
    public Phases findById(Long id) {
        return phasesRepository.findById(id).orElseThrow(() ->
                new NotFoundException("No record with id " + id + " found"));
    }

    @Override
    public Phases create(Phases phases) {
        return phasesRepository.save(phases);
    }

    @Override
    public Long delete(Long id) {
        try {
            phasesRepository.deleteById(id);
            return id;
        } catch (Exception e) {
            e.getStackTrace();
        }
        return (long) 0;
    }

    @Override
    public Phases update(Phases phases) {
        Phases foundPhases = findById(phases.getId());
        return getPhases(phases, foundPhases);
    }

    @Override
    public Phases update(Long id, Phases phases) {
        Phases foundPhases = findById(id);
        return getPhases(phases, foundPhases);
    }

    private Phases getPhases(Phases phases, Phases foundPhases) {
        foundPhases.setStartYear(phases.getStartYear());
        foundPhases.setEndYear(phases.getEndYear());
        foundPhases.setPmApproved(phases.isPmApproved());
        foundPhases.setPcApproved(phases.isPcApproved());
        foundPhases.setRegistrationFee(phases.getRegistrationFee());
        foundPhases.setBasePremium(phases.getBasePremium());
        foundPhases.setBonusContribution(phases.getBonusContribution());
        return phasesRepository.save(foundPhases);
    }
}
