package com.stipa.BoreshaMaisha.service;

import com.stipa.BoreshaMaisha.NotFoundException;
import com.stipa.BoreshaMaisha.models.Vpgs;
import com.stipa.BoreshaMaisha.repositories.VpgsRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class VpgsServiceImpl implements VpgsService {
    private final VpgsRepository vpgsRepository;

    public VpgsServiceImpl(VpgsRepository vpgsRepository) {
        this.vpgsRepository = vpgsRepository;
    }

    @Override
    public Page<Vpgs> findAll(Pageable page) {
        return vpgsRepository.findAll(page);
    }

    @Override
    public Vpgs findById(Long id) {
        return vpgsRepository.findById(id).orElseThrow(() ->
                new NotFoundException("No record with id " + id + " found"));
    }

    @Override
    public Page<Vpgs> findByName(String name, Pageable page) {
        return vpgsRepository.findByNameContainingIgnoreCase(name, page);
    }

    @Override
    public Vpgs create(Vpgs vpgs) {
        return vpgsRepository.save(vpgs);
    }

    @Override
    public Long delete(Long id) {
        try {
            vpgsRepository.deleteById(id);
            return id;
        } catch (Exception e) {
            e.getStackTrace();
        }
        return (long) 0;
    }

    @Override
    public Vpgs update(Long id, Vpgs vpgs) {
        Vpgs foundVpgs = findById(id);
        foundVpgs.setName(vpgs.getName());
        foundVpgs.setSchemes(vpgs.getSchemes());
        return vpgsRepository.save(foundVpgs);
    }
}
