package com.stipa.BoreshaMaisha.controller;

import com.stipa.BoreshaMaisha.models.Roles;
import com.stipa.BoreshaMaisha.service.RolesService;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@CrossOrigin(origins = "http://localhost:8088")
@RestController
@RequestMapping(value = "roles")
public class RolesController {
    private final RolesService rolesService;

    public RolesController(RolesService rolesService) {
        this.rolesService = rolesService;
    }

    @GetMapping
    public List<Roles> findAll() {
        return rolesService.findAll();
    }

    @GetMapping(value = "{id}")
    public Roles findById(@PathVariable String id) {
        return rolesService.findById(id);
    }

    @PostMapping
    public Roles create(@Valid @RequestBody Roles roles) {
        return rolesService.create(roles);
    }
}
