package com.stipa.BoreshaMaisha.controller;

import com.stipa.BoreshaMaisha.models.Households;
import com.stipa.BoreshaMaisha.models.Metadata;
import com.stipa.BoreshaMaisha.service.HouseholdsService;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@CrossOrigin(origins = "http://localhost:8088")
@RestController
@RequestMapping(value = "households")
public class HouseholdsController {
    private final HouseholdsService householdsService;

    public HouseholdsController(HouseholdsService householdsService) {
        this.householdsService = householdsService;
    }

    @GetMapping
    public Map<String, Object> findAll(@RequestParam int page, @RequestParam int limit) {
        Metadata metadata = new Metadata();
        limit = limit <= 0 ? 10 : limit;
        //pagination starts at 0 must read more on this
        page = page <= 0 ? 0 : page;
        Pageable pageValues = PageRequest.of(page, limit, Sort.by("id"));
        metadata.setLimit(limit);
        metadata.setPage(page);
        metadata.setTime(new Date());
        Map<String, Object> response = new HashMap<>();
        response.put("meta_data", metadata);
        response.put("data", householdsService.findAll(pageValues));
        return response;
    }

    @GetMapping(value = "{id}")
    public Households findById(@PathVariable String id) {
        return (Households) householdsService.findById(id);
    }


    @PostMapping
    public Households create(@Valid @RequestBody Households households) {
        return householdsService.create(households);
    }

    @DeleteMapping(value = "{id}")
    public Map<String, Object> delete(@PathVariable String id) {
        Map<String, Object> response = new HashMap<>();
        response.put("deleted", householdsService.delete(id));
        return response;
    }

    @PatchMapping(value = "{id}")
    public Households update(@PathVariable String id, @Valid @RequestBody Households households) {
        return householdsService.update(id, households);
    }
}
