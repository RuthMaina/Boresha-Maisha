package com.stipa.BoreshaMaisha.controller;

import com.stipa.BoreshaMaisha.models.Metadata;
import com.stipa.BoreshaMaisha.models.Vpgs;
import com.stipa.BoreshaMaisha.service.VpgsService;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@CrossOrigin(origins = "http://localhost:8088")
@RestController
@RequestMapping(value = "vpgs")
public class VpgsController {
    private final VpgsService vpgsService;

    public VpgsController(VpgsService vpgsService) {
        this.vpgsService = vpgsService;
    }

    @GetMapping
    public Map<String, Object> findByName(@RequestParam int page, @RequestParam int limit, @RequestParam String name) {
        Metadata metadata = new Metadata();
        limit = limit <= 0 ? 10 : limit;
        page = page <= 0 ? 0 : page;
        Pageable pageValues = PageRequest.of(page, limit, Sort.by("id"));
        metadata.setLimit(limit);
        metadata.setPage(page);
        metadata.setTime(new Date());
        Map<String, Object> response = new HashMap<>();
        response.put("meta_data", metadata);
        response.put("data", vpgsService.findByName(name, pageValues));
        return response;
    }

    @GetMapping(value = "/all")
    public Map<String, Object> findAll(@RequestParam int page, @RequestParam int limit) {
        Metadata metadata = new Metadata();
        limit = limit <= 0 ? 10 : limit;
        //pagination starts at 0 must read more on this
        page = page <= 0 ? 0 : page;
        Pageable pageValues = PageRequest.of(page, limit, Sort.by("id"));
        metadata.setLimit(limit);
        metadata.setPage(page);
        metadata.setTime(new Date());
        Map<String, Object> response = new HashMap<>();
        response.put("meta_data", metadata);
        response.put("data", vpgsService.findAll(pageValues));
        return response;
    }

    @GetMapping(value = "{id}")
    public Vpgs findById(@PathVariable Long id) {
        return vpgsService.findById(id);
    }


    @PostMapping
    public Vpgs create(@Valid @RequestBody Vpgs counties) {
        return vpgsService.create(counties);
    }

    @DeleteMapping(value = "{id}")
    public Map<String, Object> delete(@PathVariable Long id) {
        Map<String, Object> response = new HashMap<>();
        response.put("deleted", vpgsService.delete(id));
        return response;
    }

    @PatchMapping(value = "{id}")
    public Vpgs update(@PathVariable Long id, @Valid @RequestBody Vpgs counties) {
        return vpgsService.update(id, counties);
    }
}
