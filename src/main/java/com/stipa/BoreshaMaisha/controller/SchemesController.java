package com.stipa.BoreshaMaisha.controller;

import com.stipa.BoreshaMaisha.models.Metadata;
import com.stipa.BoreshaMaisha.models.Schemes;
import com.stipa.BoreshaMaisha.service.SchemesService;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@CrossOrigin(origins = "http://localhost:8088")
@RestController
@RequestMapping(value = "schemes")
public class SchemesController {
    private final SchemesService schemesService;

    public SchemesController(SchemesService schemesService) {
        this.schemesService = schemesService;
    }

    @GetMapping
    public Map<String, Object> findByName(@RequestParam int page, @RequestParam int limit, @RequestParam String name) {
        Metadata metadata = new Metadata();
        limit = limit <= 0 ? 10 : limit;
        page = page <= 0 ? 0 : page;
        Pageable pageValues = PageRequest.of(page, limit, Sort.by("id"));
        metadata.setLimit(limit);
        metadata.setPage(page);
        metadata.setTime(new Date());
        Map<String, Object> response = new HashMap<>();
        response.put("meta_data", metadata);
        response.put("data", schemesService.findByName(name, pageValues));
        return response;
    }

    @GetMapping(value = "/all")
    public Map<String, Object> findAll(@RequestParam int page, @RequestParam int limit) {
        Metadata metadata = new Metadata();
        limit = limit <= 0 ? 10 : limit;
        //pagination starts at 0 must read more on this
        page = page <= 0 ? 0 : page;
        Pageable pageValues = PageRequest.of(page, limit, Sort.by("id"));
        metadata.setLimit(limit);
        metadata.setPage(page);
        metadata.setTime(new Date());
        Map<String, Object> response = new HashMap<>();
        response.put("meta_data", metadata);
        response.put("data", schemesService.findAll(pageValues));
        return response;
    }

    @GetMapping(value = "{id}")
    public Schemes findById(@PathVariable Long id) {
        return schemesService.findById(id);
    }


    @PostMapping(value = "{countyId}")
    public Schemes create(@RequestBody Schemes counties, @PathVariable Long countyId) {
        return schemesService.create(counties,countyId);
    }

    @DeleteMapping(value = "{id}")
    public Map<String, Object> delete(@PathVariable Long id) {
        Map<String, Object> response = new HashMap<>();
        response.put("deleted", schemesService.delete(id));
        return response;
    }

    @PatchMapping(value = "{countyId}/{schemeId}")
    public Schemes update(@RequestBody Schemes counties,@PathVariable Long countyId,@PathVariable Long schemeId) {
        return schemesService.update(counties,countyId,schemeId);
    }
}
