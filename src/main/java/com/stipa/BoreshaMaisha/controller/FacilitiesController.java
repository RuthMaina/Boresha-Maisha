package com.stipa.BoreshaMaisha.controller;

import com.stipa.BoreshaMaisha.models.Facilities;
import com.stipa.BoreshaMaisha.models.Metadata;
import com.stipa.BoreshaMaisha.service.FacilitiesService;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@CrossOrigin(origins = "http://localhost:8088")
@RestController
@RequestMapping(value = "facilities")
public class FacilitiesController {
    private final FacilitiesService facilitiesService;

    public FacilitiesController(FacilitiesService facilitiesService) {
        this.facilitiesService = facilitiesService;
    }

    @GetMapping
    public Map<String, Object> findByAddress(@RequestParam int page, @RequestParam int limit, @RequestParam String name) {
        Metadata metadata = new Metadata();
        limit = limit <= 0 ? 10 : limit;
        page = page <= 0 ? 0 : page;
        Pageable pageValues = PageRequest.of(page, limit, Sort.by("id"));
        metadata.setLimit(limit);
        metadata.setPage(page);
        metadata.setTime(new Date());
        Map<String, Object> response = new HashMap<>();
        response.put("meta_data", metadata);
        response.put("data", facilitiesService.findByAddress(name, pageValues));
        return response;
    }

    @GetMapping(value = "{id}")
    public Facilities findById(@PathVariable Long id) {
        return facilitiesService.findById(id);
    }


    @PostMapping
    public Facilities create(@Valid @RequestBody Facilities counties) {
        return facilitiesService.create(counties);
    }

    @DeleteMapping(value = "{id}")
    public Map<String, Object> delete(@PathVariable Long id) {
        Map<String, Object> response = new HashMap<>();
        response.put("deleted", facilitiesService.delete(id));
        return response;
    }

    @PatchMapping
    public Facilities update(@Valid @RequestBody Facilities counties) {
        return facilitiesService.update(counties);
    }

    @PatchMapping(value = "{id}")
    public Facilities update(@PathVariable Long id, @Valid @RequestBody Facilities counties) {
        return facilitiesService.update(id, counties);
    }
}
