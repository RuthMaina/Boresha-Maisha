package com.stipa.BoreshaMaisha.controller;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Authorization;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class helloWorld {
    @RequestMapping({"/hello"})
    public String firstPage() {
        return "This is the home page";
    }
}
