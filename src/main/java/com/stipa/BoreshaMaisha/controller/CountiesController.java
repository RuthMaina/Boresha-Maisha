package com.stipa.BoreshaMaisha.controller;

import com.stipa.BoreshaMaisha.models.Counties;
import com.stipa.BoreshaMaisha.models.Metadata;
import com.stipa.BoreshaMaisha.service.CountiesService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@CrossOrigin(origins = "http://localhost:8088")
@RestController
@RequestMapping(value = "counties")
public class CountiesController {
    private final CountiesService countiesService;

    public CountiesController(CountiesService countiesService) {
        this.countiesService = countiesService;
    }

    @GetMapping
    public Map<String, Object> findByName(@RequestParam int page, @RequestParam int limit, @RequestParam String name) {
        Metadata metadata = new Metadata();
        limit = limit <= 0 ? 10 : limit;
        page = page <= 0 ? 0 : page;
        Pageable pageValues = PageRequest.of(page, limit, Sort.by("id"));
        metadata.setLimit(limit);
        metadata.setPage(page);
        metadata.setTime(new Date());
        Map<String, Object> response = new HashMap<>();
        Page<Counties> foundCounties = countiesService.findByName(name,pageValues);
        response.put("meta_data", metadata);
        response.put("data", foundCounties);
        return response;
    }


    @GetMapping(value = "{id}")
    public Counties findById(@PathVariable Long id) {
        return countiesService.findById(id);
    }


    @PostMapping(value = "{phaseId}")
    public Counties create(@Valid @RequestBody Counties counties,@PathVariable Long phaseId) {
        return countiesService.create(counties,phaseId);
    }

    @DeleteMapping(value = "{id}")
    public Map<String, Object> delete(@PathVariable Long id) {
        Map<String, Object> response = new HashMap<>();
        response.put("deleted", countiesService.delete(id));
        return response;
    }

    @PatchMapping(value = "{phaseId}")
    public Counties update(@Valid @RequestBody Counties counties, @PathVariable Long phaseId,@RequestParam Long countyId) {

        return countiesService.update(counties,phaseId,countyId);
    }

}
