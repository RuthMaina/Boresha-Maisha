package com.stipa.BoreshaMaisha.controller;

import com.stipa.BoreshaMaisha.models.Beneficiaries;
import com.stipa.BoreshaMaisha.models.Metadata;
import com.stipa.BoreshaMaisha.service.BeneficiariesService;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@CrossOrigin(origins = "http://localhost:8088")
@RestController
@RequestMapping(value = "beneficiaries")
public class BeneficiariesController {
    private final BeneficiariesService beneficiariesService;

    public BeneficiariesController(BeneficiariesService beneficiariesService) {
        this.beneficiariesService = beneficiariesService;
    }

    @GetMapping
    public Map<String, Object> findAll(@RequestParam int page, @RequestParam int limit) {
        Metadata metadata = new Metadata();
        limit = limit <= 0 ? 10 : limit;
        //pagination starts at 0 must read more on this
        page = page <= 0 ? 0 : page;
        Pageable pageValues = PageRequest.of(page, limit, Sort.by("id"));
        metadata.setLimit(limit);
        metadata.setPage(page);
        metadata.setTime(new Date());
        Map<String, Object> response = new HashMap<>();
        response.put("meta_data", metadata);
        response.put("data", beneficiariesService.findAll(pageValues));
        return response;
    }

    @GetMapping(value = "{id}")
    public Beneficiaries findById(@PathVariable String id) {
        return beneficiariesService.findById(id);
    }

    @PostMapping(value = "create/{schemeCode}")
    public Beneficiaries createPrinciple(@PathVariable String schemeCode, @Valid @RequestBody Beneficiaries beneficiaries) {
        return beneficiariesService.createPrinciple(schemeCode, beneficiaries);
    }

    @PostMapping(value = "register/{householdCode}")
    public Beneficiaries createBeneficiary(@PathVariable String householdCode, @Valid @RequestBody Beneficiaries beneficiaries) {
        return beneficiariesService.createBeneficiary(householdCode, beneficiaries);
    }

    @DeleteMapping
    public Map<String, Object> delete(@PathVariable String id) {
        Map<String, Object> response = new HashMap<>();
        response.put("deleted", beneficiariesService.delete(id));
        return response;
    }

    @PatchMapping(value = "{id}")
    public Beneficiaries update(@PathVariable String id, @Valid @RequestBody Beneficiaries beneficiaries) {
        return beneficiariesService.update(id, beneficiaries);
    }
}
