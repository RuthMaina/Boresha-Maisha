package com.stipa.BoreshaMaisha.controller;

import com.stipa.BoreshaMaisha.models.Metadata;
import com.stipa.BoreshaMaisha.models.Phases;
import com.stipa.BoreshaMaisha.service.PhasesService;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.Year;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@CrossOrigin(origins = "http://localhost:8088")
@RestController
@RequestMapping(value = "phases")
public class PhasesController {
    private final PhasesService phasesService;

    public PhasesController(PhasesService phasesService) {
        this.phasesService = phasesService;
    }

    @GetMapping
    public Map<String, Object> findByStartYear(@RequestParam int page, @RequestParam int limit, @RequestParam String year) {
        Metadata metadata = new Metadata();
        limit = limit <= 0 ? 10 : limit;
        page = page <= 0 ? 0 : page;
        Pageable pageValues = PageRequest.of(page, limit, Sort.by("id"));
        metadata.setLimit(limit);
        metadata.setPage(page);
        metadata.setTime(new Date());
        Map<String, Object> response = new HashMap<>();
        response.put("meta_data", metadata);
        response.put("data", phasesService.findByStartYear(year, pageValues));
        return response;
    }

    @GetMapping(value = "/all")
    public Map<String, Object> findAll(@RequestParam int page, @RequestParam int limit) {
        Metadata metadata = new Metadata();
        limit = limit <= 0 ? 10 : limit;
        //pagination starts at 0 must read more on this
        page = page <= 0 ? 0 : page;
        Pageable pageValues = PageRequest.of(page, limit, Sort.by("id"));
        metadata.setLimit(limit);
        metadata.setPage(page);
        metadata.setTime(new Date());
        Map<String, Object> response = new HashMap<>();
        response.put("meta_data", metadata);
        response.put("data", phasesService.findAll(pageValues));
        return response;
    }

    @GetMapping(value = "{id}")
    public Phases findById(@PathVariable Long id) {
        return phasesService.findById(id);
    }

    @PostMapping
    public Phases create(@Valid @RequestBody Phases phases) {
        return phasesService.create(phases);
    }

    @DeleteMapping(value = "{id}")
    public Map<String, Object> delete(@PathVariable Long id) {
        Map<String, Object> response = new HashMap<>();
        response.put("deleted", phasesService.delete(id));
        return response;
    }

    @PatchMapping
    public Phases update(@Valid @RequestBody Phases phases) {
        return phasesService.update(phases);
    }

    @PatchMapping(value = "{id}")
    public Phases update(@PathVariable Long id, @Valid @RequestBody Phases phases) {
        return phasesService.update(id, phases);
    }
}
