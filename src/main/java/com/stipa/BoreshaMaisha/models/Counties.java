package com.stipa.BoreshaMaisha.models;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.util.Set;


@Entity
@Table(name = "counties")
//@SelectBeforeUpdate
public class Counties {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(updatable = false)
    private Long id;

    @Column
    private String name;

    @Column(unique = true)
    private String code;

    @ManyToOne
    @JoinColumn(name = "phase")
//    @JsonBackReference
    private Phases phases;

    @OneToMany(mappedBy = "counties")
//    @JsonManagedReference
    @JsonIgnore
    private Set<Schemes> schemes;


    public Counties() {
    }

    public Counties(String name, String code, Phases phases, Set<Schemes> schemes) {
        this.name = name;
        this.code = code;
        this.phases = phases;
        this.schemes = schemes;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Phases getPhases() {
        return phases;
    }

    public void setPhases(Phases phases) {
        this.phases = phases;
    }

    public Set<Schemes> getSchemes() {
        return schemes;
    }

    public void setSchemes(Set<Schemes> schemes) {
        this.schemes = schemes;
    }

    public String getPhaseYears() {
        return phases.getStartYear() + " - " + phases.getEndYear();
    }

    public Long getPhaseId()
    {
        return phases.getId();
    }



    @Override
    public String toString() {
        return "{" +
                "id:" + id +
                ", name:'" + name + '\'' +
                ", code:'" + code + '\'' +
                '}';
    }
}
