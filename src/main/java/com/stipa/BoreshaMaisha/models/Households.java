package com.stipa.BoreshaMaisha.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.stipa.BoreshaMaisha.utils.GenerateHouseholdId;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.SelectBeforeUpdate;

import javax.persistence.*;
import java.util.Set;

//@Getter
//@Setter
//@ToString
//@RequiredArgsConstructor
//@AllArgsConstructor(access = AccessLevel.PROTECTED)
@Entity
@Table(name = "households")
//@SelectBeforeUpdate
public class Households {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "household_seq")
    @GenericGenerator(
            name = "household_seq",
            strategy = "com.stipa.BoreshaMaisha.utils.GenerateHouseholdId",
            parameters = {
                    @Parameter(name = GenerateHouseholdId.INCREMENT_PARAM, value = "1"),
                    @Parameter(name = GenerateHouseholdId.CODE_NUMBER_SEPARATOR_PARAMETER, value = ""),
                    @Parameter(name = GenerateHouseholdId.NUMBER_FORMAT_PARAMETER, value = "%04d")
            })
    @Column(updatable = false)
    private String id;

    @ManyToOne
    @JoinColumn(name = "scheme")
//    @JsonBackReference
    private Schemes schemes;

    @ManyToOne
    @JoinColumn(name = "vpg")
//    @JsonBackReference
    private Vpgs vpgs;

    @OneToMany(mappedBy = "households")
//    @JsonManagedReference
    @JsonIgnore
    private Set<Beneficiaries> beneficiaries;

    public Households() {
    }

    public Households(Schemes schemes, Vpgs vpgs, Set<Beneficiaries> beneficiaries) {
        this.schemes = schemes;
        this.vpgs = vpgs;
        this.beneficiaries = beneficiaries;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Schemes getSchemes() {
        return schemes;
    }

    public void setSchemes(Schemes schemes) {
        this.schemes = schemes;
    }

    public Vpgs getVpgs() {
        return vpgs;
    }

    public void setVpgs(Vpgs vpgs) {
        this.vpgs = vpgs;
    }

    public Set<Beneficiaries> getBeneficiaries() {
        return beneficiaries;
    }

    public void setBeneficiaries(Set<Beneficiaries> beneficiaries) {
        this.beneficiaries = beneficiaries;
    }

    @Override
    public String toString() {
        return "Households{" +
                "id='" + id + '\'' +
                ", schemes=" + schemes +
                ", vpgs=" + vpgs +
                ", beneficiaries=" + beneficiaries +
                '}';
    }
}
