package com.stipa.BoreshaMaisha.models;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.*;
import org.hibernate.annotations.SelectBeforeUpdate;

import javax.persistence.*;
import java.util.Set;

//@Getter
//@Setter
//@ToString
//@RequiredArgsConstructor
//@AllArgsConstructor(access = AccessLevel.PROTECTED)
@Entity
@Table(name = "vpgs")
public class Vpgs {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(updatable = false)
    private Long id;

    @Column(name = "name")
    private String name;

    @ManyToOne
    @JoinColumn(name = "scheme")
//    @JsonBackReference
    private Schemes schemes;

    @OneToMany(mappedBy = "vpgs")
//    @JsonManagedReference
    @JsonIgnore
    private Set<Households> households;

    public Vpgs() {
    }

    public Vpgs(String name, Schemes schemes, Set<Households> households) {
        this.name = name;
        this.schemes = schemes;
        this.households = households;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Schemes getSchemes() {
        return schemes;
    }

    public void setSchemes(Schemes schemes) {
        this.schemes = schemes;
    }

    public Set<Households> getHouseholds() {
        return households;
    }

    public void setHouseholds(Set<Households> households) {
        this.households = households;
    }

    @Override
    public String toString() {
        return "Vpgs{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", schemes=" + schemes +
                ", households=" + households +
                '}';
    }
}
