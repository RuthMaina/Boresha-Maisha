package com.stipa.BoreshaMaisha.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cascade;
import org.apache.commons.text.WordUtils;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "user_roles")
public class Roles {
    private String id;
    private String role;

    @JsonIgnore
    @ManyToMany(fetch = FetchType.LAZY,
            mappedBy = "role")
    private Set<Users> users;

    public Roles() {
    }

    public Roles(String role) {
        this.id = role.toLowerCase();
        this.role = WordUtils.capitalizeFully(role, ' ', '_', '-', '.');
    }

    public Roles(Roles roles){
    }

    public Roles(String id, String role, Set<Users> users) {
        this.id = id;
        this.role = role;
        this.users = users;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = WordUtils.capitalizeFully(role, ' ', '_', '-', '.');
    }

    public Set<Users> getUsers() {
        return users;
    }

    public void setUsers(Set<Users> users) {
        this.users = users;
    }

    @Override
    public String toString() {
        return "Roles{" +
                "id='" + id + '\'' +
                ", role='" + role + '\'' +
                '}';
    }
}