package com.stipa.BoreshaMaisha.models;

import lombok.*;
import org.hibernate.annotations.SelectBeforeUpdate;

import javax.persistence.*;

//@Getter
//@Setter
//@ToString
//@RequiredArgsConstructor
//@AllArgsConstructor(access = AccessLevel.PROTECTED)
@Entity
@Table(name = "facilities")
//@SelectBeforeUpdate
public class Facilities {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(updatable = false)
    private Long id;

    private String level;

    @Column(unique = true)
    private String address;

    @Column(name = "contact_person")
    private String contactPerson;

    @Column(name = "payment_terms")
    private String settlementTerms;

    private String services;

    // Pending or signed
    @Column(name = "mou_status")
    private String mouStatus;

    @Column(name = "mou_link")
    private String mouLink;

    @Column(name = "referral_facility")
    private String referralFacility;

    public Facilities() {
    }

    public Facilities(String level, String address, String contactPerson, String settlementTerms, String services, String mouStatus, String mouLink, String referralFacility) {
        this.level = level;
        this.address = address;
        this.contactPerson = contactPerson;
        this.settlementTerms = settlementTerms;
        this.services = services;
        this.mouStatus = mouStatus;
        this.mouLink = mouLink;
        this.referralFacility = referralFacility;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getContactPerson() {
        return contactPerson;
    }

    public void setContactPerson(String contactPerson) {
        this.contactPerson = contactPerson;
    }

    public String getSettlementTerms() {
        return settlementTerms;
    }

    public void setSettlementTerms(String settlementTerms) {
        this.settlementTerms = settlementTerms;
    }

    public String getServices() {
        return services;
    }

    public void setServices(String services) {
        this.services = services;
    }

    public String getMouStatus() {
        return mouStatus;
    }

    public void setMouStatus(String mouStatus) {
        this.mouStatus = mouStatus;
    }

    public String getMouLink() {
        return mouLink;
    }

    public void setMouLink(String mouLink) {
        this.mouLink = mouLink;
    }

    public String getReferralFacility() {
        return referralFacility;
    }

    public void setReferralFacility(String referralFacility) {
        this.referralFacility = referralFacility;
    }

    @Override
    public String toString() {
        return "Facilities{" +
                "id=" + id +
                ", level='" + level + '\'' +
                ", address='" + address + '\'' +
                ", contactPerson='" + contactPerson + '\'' +
                ", settlementTerms='" + settlementTerms + '\'' +
                ", services='" + services + '\'' +
                ", mouStatus='" + mouStatus + '\'' +
                ", mouLink='" + mouLink + '\'' +
                ", referralFacility='" + referralFacility + '\'' +
                '}';
    }
}
