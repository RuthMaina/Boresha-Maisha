package com.stipa.BoreshaMaisha.models;

import lombok.*;

import java.util.Date;

//@Getter
//@Setter
//@ToString
//@RequiredArgsConstructor
//@AllArgsConstructor(access = AccessLevel.PROTECTED)
public class Metadata {
    private Date time;

    private int page;

    private int limit;

    public Metadata() {
    }

    public Metadata(Date time, int page, int limit) {
        this.time = time;
        this.page = page;
        this.limit = limit;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    @Override
    public String toString() {
        return "Metadata{" +
                "time=" + time +
                ", page=" + page +
                ", limit=" + limit +
                '}';
    }
}
