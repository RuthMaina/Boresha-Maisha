package com.stipa.BoreshaMaisha.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.*;
import org.hibernate.annotations.SelectBeforeUpdate;

import javax.persistence.*;
import java.time.Year;
import java.util.Set;

//@Getter
//@Setter
//@ToString
//@RequiredArgsConstructor
//@AllArgsConstructor(access = AccessLevel.PROTECTED)
@Entity
@Table(name = "phases")
//@SelectBeforeUpdate
public class Phases {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(updatable = false)
    private Long id;

    @Column(name = "start_year")
    private String startYear;

    @Column(name = "end_year")
    private String endYear;

    @Column(name = "pm_approved")
    private boolean pmApproved;

    @Column(name = "pc_approved")
    private boolean pcApproved;

    @Column(name = "registration_fee")
    private int registrationFee;

    @Column(name = "base_premium")
    private int basePremium;

    @Column(name = "bonus_contribution")
    private int bonusContribution;

    @OneToMany(mappedBy = "phases")
//    @JsonManagedReference
    @JsonIgnore
    private Set<Counties> counties;

    public Phases() {
    }

    public Phases(String startYear, String endYear, boolean pmApproved, boolean pcApproved, int registrationFee, int basePremium, int bonusContribution, Set<Counties> counties) {
        this.startYear = startYear;
        this.endYear = endYear;
        this.pmApproved = pmApproved;
        this.pcApproved = pcApproved;
        this.registrationFee = registrationFee;
        this.basePremium = basePremium;
        this.bonusContribution = bonusContribution;
        this.counties = counties;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getStartYear() {
        return startYear;
    }

    public void setStartYear(String startYear) {
        this.startYear = startYear;
    }

    public String getEndYear() {
        return endYear;
    }

    public void setEndYear(String endYear) {
        this.endYear = endYear;
    }

    public boolean isPmApproved() {
        return pmApproved;
    }

    public void setPmApproved(boolean pmApproved) {
        this.pmApproved = pmApproved;
    }

    public boolean isPcApproved() {
        return pcApproved;
    }

    public void setPcApproved(boolean pcApproved) {
        this.pcApproved = pcApproved;
    }

    public int getRegistrationFee() {
        return registrationFee;
    }

    public void setRegistrationFee(int registrationFee) {
        this.registrationFee = registrationFee;
    }

    public int getBasePremium() {
        return basePremium;
    }

    public void setBasePremium(int basePremium) {
        this.basePremium = basePremium;
    }

    public int getBonusContribution() {
        return bonusContribution;
    }

    public void setBonusContribution(int bonusContribution) {
        this.bonusContribution = bonusContribution;
    }

    public Set<Counties> getCounties() {
        return counties;
    }

    public void setCounties(Set<Counties> counties) {
        this.counties = counties;
    }

    @Override
    public String toString() {
        return "Phases{" +
                "id=" + id +
                ", startYear=" + startYear +
                ", endYear=" + endYear +
                ", pmApproved=" + pmApproved +
                ", pcApproved=" + pcApproved +
                ", registrationFee=" + registrationFee +
                ", basePremium=" + basePremium +
                ", bonusContribution=" + bonusContribution +
                ", counties=" + counties +
                '}';
    }
}
