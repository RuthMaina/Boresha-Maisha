package com.stipa.BoreshaMaisha.models;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.hibernate.annotations.SelectBeforeUpdate;
import springfox.documentation.spring.web.json.Json;

import javax.persistence.*;
import java.util.Set;

//@Getter
//@Setter
//@ToString
//@RequiredArgsConstructor
//@AllArgsConstructor(access = AccessLevel.PROTECTED)
@Entity
@Table(name = "facilities")
//@SelectBeforeUpdate
public class Schemes {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(updatable = false)
    private Long id;

    @Column(unique = true)
    private String name;

    @ManyToOne
    @JoinColumn(name = "county")
//    @JsonBackReference
    private Counties counties;

    @Column(name = "sub_county")
    private String subCounty;

    private String village;

    private String code;

    @OneToMany(mappedBy = "schemes")
    @JsonIgnore
    private Set<Vpgs> vpgs;

    public Schemes() {
    }

    public Schemes(String name, Counties counties, String subCounty, String village, String code, Set<Vpgs> vpgs) {
        this.name = name;
        this.counties = counties;
        this.subCounty = subCounty;
        this.village = village;
        this.code = code;
        this.vpgs = vpgs;
    }

    public String getCountyData()
    {
        return counties.toString();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Counties getCounties() {
        return counties;
    }

    public void setCounties(Counties counties) {
        this.counties = counties;
    }

    public String getSubCounty() {
        return subCounty;
    }

    public void setSubCounty(String subCounty) {
        this.subCounty = subCounty;
    }

    public String getVillage() {
        return village;
    }

    public void setVillage(String village) {
        this.village = village;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Set<Vpgs> getVpgs() {
        return vpgs;
    }

    public void setVpgs(Set<Vpgs> vpgs) {
        this.vpgs = vpgs;
    }

    @Override
    public String toString() {
        return "Schemes{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", counties=" + counties +
                ", subCounty='" + subCounty + '\'' +
                ", village='" + village + '\'' +
                ", code='" + code + '\'' +
                ", vpgs=" + vpgs +
                '}';
    }
}
