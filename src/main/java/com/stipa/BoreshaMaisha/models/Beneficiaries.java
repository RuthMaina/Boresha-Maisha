package com.stipa.BoreshaMaisha.models;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.stipa.BoreshaMaisha.enums.Gender;
import com.stipa.BoreshaMaisha.enums.MaritalStatus;
import com.stipa.BoreshaMaisha.utils.GenerateBeneficiaryId;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

//@Getter
//@Setter
//@ToString
//@RequiredArgsConstructor
//@AllArgsConstructor(access = AccessLevel.PROTECTED)
@Entity
@Table(name = "beneficiaries")
public class Beneficiaries {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "beneficiary_seq")
    @GenericGenerator(
            name = "beneficiary_seq",
            strategy = "com.stipa.BoreshaMaisha.utils.GenerateBeneficiaryId",
            parameters = {
                    @Parameter(name = GenerateBeneficiaryId.INCREMENT_PARAM, value = "1"),
                    @Parameter(name = GenerateBeneficiaryId.CODE_NUMBER_SEPARATOR_PARAMETER, value = "/"),
                    @Parameter(name = GenerateBeneficiaryId.NUMBER_FORMAT_PARAMETER, value = "%02d")
            })
    @Column(updatable = false)
    private String id;

    @ManyToOne
    @JoinColumn(name = "household")
//    @JsonBackReference
    private Households households;

    @Column(name = "photo")
    private String photo;

    @Column(nullable = false)
    private String names;

    @Column(name = "id_number")
    private Long idNumber;

    @Column(name = "date_of_birth")
    @Temporal(TemporalType.DATE)
    private Date dateOfBirth;

    private int age;

    @Column(nullable = false)
    private String gender;

    @Column(name = "marital_status")
    private String maritalStatus;

    private String phone;

    @Column(name = "income_level")
    private String incomeLevel;

    @Column(name = "education_level")
    private String educationLevel;

    private String livelihood;

    @Column(name = "vsl_participation")
    private String vslParticipation;

    @Column(name = "agriculture_participation")
    private String agricultureParticipation;

    @Column(name = "uhc_membership")
    private boolean UhcMembership;

    @Column(name = "uhc_number")
    private Long UhcNUmber;

    @Column(name = "nhif_membership")
    private boolean NhifMembership;

    @Column(name = "nhif_number")
    private Long NhifNumber;

    private String role;

    public Beneficiaries() {
    }

    public Beneficiaries(Households households, String photo, String names, Long idNumber, Date dateOfBirth, int age, String gender, String maritalStatus, String phone, String incomeLevel, String educationLevel, String livelihood, String vslParticipation, String agricultureParticipation, boolean uhcMembership, Long uhcNUmber, boolean nhifMembership, Long nhifNumber, String role) {
        this.households = households;
        this.photo = photo;
        this.names = names;
        this.idNumber = idNumber;
        this.dateOfBirth = dateOfBirth;
        this.age = age;
        this.gender = gender;
        this.maritalStatus = maritalStatus;
        this.phone = phone;
        this.incomeLevel = incomeLevel;
        this.educationLevel = educationLevel;
        this.livelihood = livelihood;
        this.vslParticipation = vslParticipation;
        this.agricultureParticipation = agricultureParticipation;
        UhcMembership = uhcMembership;
        UhcNUmber = uhcNUmber;
        NhifMembership = nhifMembership;
        NhifNumber = nhifNumber;
        this.role = role;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Households getHouseholds() {
        return households;
    }

    public void setHouseholds(Households households) {
        this.households = households;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getNames() {
        return names;
    }

    public void setNames(String names) {
        this.names = names;
    }

    public Long getIdNumber() {
        return idNumber;
    }

    public void setIdNumber(Long idNumber) {
        this.idNumber = idNumber;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getMaritalStatus() {
        return maritalStatus;
    }

    public void setMaritalStatus(String maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getIncomeLevel() {
        return incomeLevel;
    }

    public void setIncomeLevel(String incomeLevel) {
        this.incomeLevel = incomeLevel;
    }

    public String getEducationLevel() {
        return educationLevel;
    }

    public void setEducationLevel(String educationLevel) {
        this.educationLevel = educationLevel;
    }

    public String getLivelihood() {
        return livelihood;
    }

    public void setLivelihood(String livelihood) {
        this.livelihood = livelihood;
    }

    public String getVslParticipation() {
        return vslParticipation;
    }

    public void setVslParticipation(String vslParticipation) {
        this.vslParticipation = vslParticipation;
    }

    public String getAgricultureParticipation() {
        return agricultureParticipation;
    }

    public void setAgricultureParticipation(String agricultureParticipation) {
        this.agricultureParticipation = agricultureParticipation;
    }

    public boolean isUhcMembership() {
        return UhcMembership;
    }

    public void setUhcMembership(boolean uhcMembership) {
        UhcMembership = uhcMembership;
    }

    public Long getUhcNUmber() {
        return UhcNUmber;
    }

    public void setUhcNUmber(Long uhcNUmber) {
        UhcNUmber = uhcNUmber;
    }

    public boolean isNhifMembership() {
        return NhifMembership;
    }

    public void setNhifMembership(boolean nhifMembership) {
        NhifMembership = nhifMembership;
    }

    public Long getNhifNumber() {
        return NhifNumber;
    }

    public void setNhifNumber(Long nhifNumber) {
        NhifNumber = nhifNumber;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    @Override
    public String toString() {
        return "Beneficiaries{" +
                "id=" + id +
                ", households=" + households +
                ", photo='" + photo + '\'' +
                ", names='" + names + '\'' +
                ", idNumber=" + idNumber +
                ", dateOfBirth=" + dateOfBirth +
                ", age=" + age +
                ", gender='" + gender + '\'' +
                ", maritalStatus='" + maritalStatus + '\'' +
                ", phone='" + phone + '\'' +
                ", incomeLevel='" + incomeLevel + '\'' +
                ", educationLevel='" + educationLevel + '\'' +
                ", livelihood='" + livelihood + '\'' +
                ", vslParticipation='" + vslParticipation + '\'' +
                ", agricultureParticipation='" + agricultureParticipation + '\'' +
                ", UhcMembership=" + UhcMembership +
                ", UhcNUmber=" + UhcNUmber +
                ", NhifMembership=" + NhifMembership +
                ", NhifNumber=" + NhifNumber +
                ", role='" + role + '\'' +
                '}';
    }
}