package com.stipa.BoreshaMaisha.repositories;

import com.stipa.BoreshaMaisha.models.Schemes;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface SchemesRepository extends JpaRepository<Schemes, Long> {

    Page<Schemes> findAll(Pageable pageable);

    Page<Schemes> findByNameContainingIgnoreCase(String boo, Pageable pageable);

    Optional<Schemes> findByCode(String code);
}
