package com.stipa.BoreshaMaisha.repositories;

import com.stipa.BoreshaMaisha.models.Facilities;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FacilitiesRepository extends JpaRepository<Facilities, Long> {

    Page<Facilities> findAll(Pageable pageable);

    Page<Facilities> findByAddressContainingIgnoreCase(String boo, Pageable pageable);
}
