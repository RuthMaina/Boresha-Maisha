package com.stipa.BoreshaMaisha.repositories;

import com.stipa.BoreshaMaisha.models.Households;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface HouseholdsRepository extends JpaRepository<Households, Long> {

    Page<Households> findAll(Pageable pageable);

    Optional<Households> findById(String id);

    void deleteById(String id);

    @Query(value = "SELECT id FROM households WHERE id LIKE CONCAT('%',:keyword,'%') order by id desc limit 1", nativeQuery = true)
    String lastHousehold(@Param("keyword") String schemeCode);
}

