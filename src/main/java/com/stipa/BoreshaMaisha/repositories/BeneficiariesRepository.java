package com.stipa.BoreshaMaisha.repositories;

import com.stipa.BoreshaMaisha.models.Beneficiaries;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface BeneficiariesRepository extends JpaRepository<Beneficiaries, Long> {

    Page<Beneficiaries> findAll(Pageable pageable);

    Optional<Beneficiaries> findById(String id);

    void deleteById(String id);

    Optional<Beneficiaries> findByIdNumber(Long idNumber);

    @Query(value = "SELECT id FROM beneficiaries WHERE id LIKE CONCAT('%',:keyword,'%') order by id desc limit 1", nativeQuery = true)
    String lastBeneficiary(@Param("keyword") String schemeCode);
}