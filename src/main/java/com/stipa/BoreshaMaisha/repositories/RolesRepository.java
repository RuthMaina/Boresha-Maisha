package com.stipa.BoreshaMaisha.repositories;

import com.stipa.BoreshaMaisha.models.Roles;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface RolesRepository extends JpaRepository<Roles, Long> {
    Optional<Object> findById(String id);

    void deleteById(String id);
}
