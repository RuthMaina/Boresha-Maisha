package com.stipa.BoreshaMaisha.repositories;

import com.stipa.BoreshaMaisha.models.Phases;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.Year;

@Repository
public interface PhasesRepository extends JpaRepository<Phases, Long> {

    Page<Phases> findAll(Pageable pageable);
    Page<Phases> findByStartYearContaining(String startYear, Pageable pageable);
}
