package com.stipa.BoreshaMaisha.repositories;

import com.stipa.BoreshaMaisha.models.Counties;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CountiesRepository extends JpaRepository<Counties, Long> {

    Page<Counties> findAll(Pageable pageable);

    Page<Counties> findByNameContainingIgnoreCase(String boo, Pageable pageable);

}