package com.stipa.BoreshaMaisha.repositories;

import com.stipa.BoreshaMaisha.models.Vpgs;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface VpgsRepository extends JpaRepository<Vpgs, Long> {

    Page<Vpgs> findAll(Pageable pageable);

    Page<Vpgs> findByNameContainingIgnoreCase(String boo, Pageable pageable);
}
